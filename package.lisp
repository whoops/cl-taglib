;;;; package.lisp

(defpackage #:cl-taglib
  (:use #:cl #:cffi)
  (:nicknames #:taglib)
  (:export #:file-new
	   #:file-free
	   #:file-save
	   #:with-tag-file
	   #:file-audioproperties
	   #:file-tag
	   #:tag-artist
	   #:tag-album
	   #:tag-title
	   #:tag-comment
	   #:tag-genre
	   #:tag-track
	   #:tag-year
	   #:extract-tags
	   #:audio-length
	   #:audio-bitrate
	   #:audio-samplerate
	   #:audio-channels))
