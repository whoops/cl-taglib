;;;; cl-taglib.asd

(asdf:defsystem #:cl-taglib
  :serial t
  :description "CFFI bindings to taglib"
  :author "Walton Hoops <me@waltonhoops.com>"
  :license "BSD"
  :version "0.0.1"
  :components ((:file "package")
               (:file "cl-taglib" :depends-on ("package")))
  :depends-on (#:cffi))
