# CL-Taglib

Simple CFFI bindings to [taglib](http://taglib.github.com/)

## Quick Examples

### Reading file tags

    :::lisp
    (use-package :taglib)

    ;;; Easy way:
    (extract-tags "09 My Generation.mp3")
    ;;; result
    (:TRACK 9 :YEAR 1994 :GENRE "Rock" :COMMENT ""
     :ALBUM "Thirty Years Of Maximum R&B" :ARTIST "The Who" :TITLE "My Generation")

    ;;; Longer way:
    (with-tag-file (file "07 - 17.mp3")
      (let ((tag (file-tag file))
            (audio (file-audioproperties file)))
        (values (list (tag-title tag)
                      (tag-artist tag)
                      (tag-album tag)
                      (tag-comment tag)
                      (tag-genre tag)
                      (tag-track tag)
                      (tag-year tag))
                (list (audio-length audio)
                      (audio-bitrate audio)
                      (audio-samplerate audio)
                      (audio-channels audio)))))
    ;;; result
    ("17" "Kings Of Leon" "Only By The Night" "Sample Commment"
          "Alternative Rock" 7 2008)
    (185 247 44100 2)

### Setting tags

    :::lisp
    (use-package :taglib)
    (with-tag-file (file "turtles.mp3")
           (let ((tag (file-tag file)))
             (setf (tag-comment tag) "turtles all the way down")
             (file-save file)))

## API

(*file-new* pathname) Takes a pathname or a string and returns the tagfile.  The the tagfile MUST be freed manually, use *with-open-tagfile* instead

(*file-free* tagfile) Frees the provided tagfile, returns nil.

(*file-save* tagfile) Saves any changes to the files tags.  Returns T on success, nil on failure.

(*with-tag-file* (var-name pathname) &body) Opens the file, runs &body and closes it, similar to with-open-file.  The file is NOT automatically saved.
### Working with tags

(*file-tag* tagfile) Returns the tag structure associated with the file.  Will be automatically be freed along with the file.

(*tag-<property>* tag) Returns the given property.  All tag operations are setf-able.  Available accessors are (title album artist genre comment track year)

(*extract-tags* pathname) Returns a plist of the tags in the given file.

### Reading audio information

(*file-audioproperties* file) returns the audio properties for the given file

(*audio-<property>* audio) Returns the given property as an integer.  Properties are (length bitrate samplerate channels).  Audio properties are NOT setf-able.

## License

Copyright (c) 2013, Walton Hoops
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.